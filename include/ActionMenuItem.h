#ifndef ACTIONMENUITEM_H
#define ACTIONMENUITEM_H

#include "projectlibs.h"
#include "MenuItem.h"


class ActionMenuItem : public MenuItem
{
    public:
        ActionMenuItem(string name): MenuItem(name) {}
        virtual bool do_stuff();
};

#endif // ACTIONMENUITEM_H
