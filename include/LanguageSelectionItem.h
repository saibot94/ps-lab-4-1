#ifndef LANGUAGESELECTIONITEM_H
#define LANGUAGESELECTIONITEM_H
#include "MenuItem.h"
#include "languages.h"

class LanguageSelectionItem : public MenuItem
{
    public:
        LanguageSelectionItem(string name): MenuItem(name)
        {

        }
        virtual ~LanguageSelectionItem();
        virtual bool do_stuff();
    protected:

    private:
};

#endif // LANGUAGESELECTIONITEM_H
