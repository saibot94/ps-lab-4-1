#pragma once
#include "projectlibs.h"

namespace languages {
    void change_language(string lang_code);
    string translate(string label);
}
