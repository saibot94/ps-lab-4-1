#include "SubmenuItem.h"

/* TODO

    Use this function in order to validate if a user selection is correct or not.
    To read a user selection, write something like: cin >> option.
*/
bool SubmenuItem::valid_option(int i)
{
    if(i < 0 || (unsigned)i >= menu_items->size()) {
        return false;
    }
    return true;
}

/* TODO

Implement this
*/
bool SubmenuItem::do_stuff()
{
    int option;
    bool exit = false;
    while(!exit) {
        while(!valid_option(option-1)) {
            cout << "- " << languages::translate(get_name()) << endl;
            cout << "==================" << endl;
            menu_items->size();
            menu_items[0];
            for(int i = 0; i < menu_items->size(); i++) {
                cout << i+1 << ". "
                    << languages::translate((*menu_items)[i]->get_name())
                    << endl;
            }

            cout << "Choose your option: ";
            cin >> option;
        }
        exit = (*menu_items)[option-1]->do_stuff();
        option = 0;
    }
    return false;
 }
