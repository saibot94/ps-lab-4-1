#include "languages.h"

string CURRENT_LANGUAGE("en");
map<string, string>* read_language_file(string lang_code);

map<string, map<string, string>* >* language_map
= new map<string, map<string, string>* >();

void languages::change_language(string lang_code) {
    if(language_map->find(lang_code) == language_map->end()) {
        (*language_map)[lang_code] = read_language_file(lang_code);
    }
    CURRENT_LANGUAGE = lang_code;
}

string languages::translate(string label) {
    map<string, string>* lang =
        language_map->find(CURRENT_LANGUAGE)->second;
    if(lang->find(label) != lang->end()) {
        return lang->find(label)->second;
    } else {
        return label;
    }
}

map<string, string>* read_language_file(string lang_code)
{
    string full_name = lang_code + string(".txt");
    map<string, string>* lang = new map<string, string>();
    ifstream f;
    string line;
    f.open(full_name.c_str());

    while(!f.eof()) {
        getline(f, line);
        int pos = line.find("=");
        if(pos != string::npos) {
            string key = line.substr(0, pos);
            string value = line.substr(pos+1);
            (*lang)[key] = value;
        }
    }
    f.close();
    return lang;
}


